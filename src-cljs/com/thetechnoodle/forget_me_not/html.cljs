(ns com.thetechnoodle.forget-me-not.html
  (:require-macros [hiccups.core :as hiccups :refer [html]])
  (:require [hiccups.runtime :as hiccups]
   [com.thetechnoodle.forget-me-not.the-flower :as flower]
   [goog.dom :as gdom]
   [oops.core :refer [oset!]]))


(defn PageTitle []
   [:div.top_container
    [:h1 "Forget Me Not"]
    [:h3 "(a flower oracle)"]])

(defn AnswerBanner [value]
  (when (not= value :begin)
     (list
      [:div.answer_banner]
      (when (= value :interim)
        [:h4 "Awaiting your first selection.."]
        [:h4 ""
         [:span {:class (case value
                          :y "so"
                          :n "not"
                          nil)}
          (case value
            :y "yes, it is so"
            :n "no, it is not so"
            nil)]]))))

(defn GameOverBanner [result]
   [:div.result_banner
    [:div
     {:class (case result
               (:yes-end :no-end) "alert alert-complete"
               (:ready-set :playing) "alert alert-dark"
               nil)}
     [:h3.alert-heading
      (case result
        :ready-set "Think of your question, the oracle will give yes and no responses. Consult to begin."
        :playing (list "Holding your question in mind, move to the dark section of one petal then click."
                       [:br]
                       "The consultation is complete only when all petals have been removed.")
        :yes-end "The oracle believes the simple answer to your question is \"Yes\"."
        :no-end "The oracle believes the simple answer to your question is \"No\"."
        nil)]

     (when (not= :playing result)
       [:button.button {:id (case result
                              (:no-end :yes-end) "newGameButton"
                              "playGameButton")}
        (case result
          :ready-set "Consult"
          :yes-end "Consult again"
          :no-end "Consult again"
          nil)])]])



(defn LoadFlower [result petals]
   [:div.main_canvas
    (flower/flower-svg result petals)])


(hiccups/defhtml ForgetMeNot
  [{:keys [answer-value result flower]}]
  ; 'map key destructuring' same as:
  ; [app-state] - param
  ; (let [answer-value (:answer-value app-state)
  ;       result (:result app-state)
  ;       flower (:flower app-state)
  (PageTitle)
  (GameOverBanner result)
  (when (= result :playing)
    (AnswerBanner answer-value))
  (LoadFlower result flower))
