(ns com.thetechnoodle.forget-me-not.game-logic)


(def pre-game-flower
  [:b :b :b :b :b :b :b :b])

(def finished-flower
  [:e :e :e :e :e :e :e :e])

(defn game-flower []
  (shuffle (list :y :n :y :n :y :n :y :n)))

(defn game-status
  [{:keys [answer-value result flower]}]
  (every? #(= :e %) flower))
