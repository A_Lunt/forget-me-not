(ns com.thetechnoodle.forget-me-not.core
  (:require [goog.dom :as gdom]
            [goog.functions :as gfunctions]
            [oops.core :refer [ocall oget oset!]]
            [com.thetechnoodle.forget-me-not.html :as html]
            [com.thetechnoodle.forget-me-not.game-logic :as gamelogic]))


; this function replaces all the html
(defn set-blues-html!
  [html-str]
  (let [el (gdom/getElement "appContainer")]
    (oset! el "innerHTML" html-str)))

; possible states:
; ⚘answer-value :interim (not set), :y, :n
; ⚘result :ready-set (..go!) :playing :yes-end :no-end
; ⚘flower this is to be updated as clicked, start
; with all :b (begin), then populate with either :y or :n
; or :e (empty) represents clicked-and-removed during gameplay
(def initial-state
  {:answer-value :interim
   :result :ready-set
   ;:flower temp-flower-eg})
   :flower gamelogic/pre-game-flower})

(defonce state (atom initial-state))

;underscore prefix of first 3 arguments in the below
; indicates "don't care" about them
(defn render-ui! [_ _kwd _prev-state new-state]
  (set-blues-html! (html/ForgetMeNot new-state)))

(defn add-watchers! []
 (add-watch state :render-ui render-ui!))


;this fn returns nil if the petal is not available (i.e. val is :e or :b)
(defn game-petal-click! [petal-num val]
  (let [current-state @state
        petal-idx (dec petal-num)
           petal-clicked (get-in current-state [:flower petal-idx])
           petal-available? (and (not= :e petal-clicked)
                                 (not= :b petal-clicked))]

    (when petal-available?
     (swap! state (fn [state]
                    (-> state ; threading macro enables sequential calls below
                      (assoc :answer-value petal-clicked)
                      (assoc-in [:flower petal-idx] :e))))
     (when (= true (gamelogic/game-status @state))
       (swap! state (fn [state]
                      (assoc state :result
                             (case petal-clicked
                               :n :no-end
                               :y :yes-end
                               nil))))))))


(defn click-petal-el [el]
  (let [petal-num (int (oget el "dataset.idx"))
        curr-val (case (str (oget el "classList"))
                   "petal no" :n
                   "petal yes" :y
                   "petal begin" :b
                   "petal empty" :e)]
    (game-petal-click! petal-num curr-val)))
    ;(js/console.log (str "petal number: " petal-num))))


(defn click-app-container [js-evt]
  (let [target-el (oget js-evt "target")
        petal? (ocall (oget target-el "classList") "contains" "petal")
        new-game-btn? (= (oget target-el "id") "newGameButton")
        play-game-btn? (= (oget target-el "id") "playGameButton")]
    (when petal?
      (click-petal-el target-el))
    (when new-game-btn?
      (reset! state initial-state))
    (when play-game-btn?
      (swap! state (fn [state]
                     (-> state
                         (assoc :flower (gamelogic/game-flower))
                         (assoc :result :playing)))))))

; don't want to keep adding events as updates occur, once will suffice.
; need to add the listener to a DOM element that isn't constantly being
; destroyed through the set-blues-html! function defined above
(def add-events!
  (gfunctions/once
   (fn []
     (ocall (gdom/getElement "appContainer") "addEventListener" "click" click-app-container))))

(def init!
  (gfunctions/once
   (fn []
     (println "Initialising the power of the flower ⚘ ⚘ ⚘")
     (add-events!)
     (add-watchers!)
     ;next line performs the initial render
     (swap! state identity))))

(ocall js/window "addEventListener" "load" init!)
