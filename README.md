--------------------
Forget Me Not
--------------------
The Forget Me Not (a flower oracle) game asks you to think of a question with a yes or no answer before selecting the consult button. You remove the individual petals by clicking on them. Once all petals are removed, the oracle delivers its response based on your interaction. You can always ask the question again if you don't like the first response!

The game is an interpretation of "s/he loves me, s/he loves me not", the sing-song petal-plucking game of chance many of us learnt as children. A short wikipedia article claims the game to be of French origin.

I created Forget Me Not (a flower oracle) with ClojureScript, using the shadow-cljs development environment within the Atom editor (using Parinfer). It consists of 4 .cljs files, a single .html file and external .css and the shadow-cljs.edn file.

⚘ The initialiser function and the game state atom is in core.cljs

⚘ I used the hiccups library in html.cljs for html creation and reloading as the game progresses

⚘ The 8-petalled flower itself is defined as an svg in the_flower.cljs

⚘ The simple logic of the game is in game_logic.cljs

You will be able to play it online shortly.

--------------------
Post-development Reflection
--------------------
Forget Me Not (a flower oracle) is the first application I have developed with ClojureScript. It has been an excellent exercise for consolidating my learning of the syntax and the technique of separating elements of the application into namespaces. A major breakthrough came with realising the technique of passing the state through functions, using map key destructuring.

I am glad of the choice of a simple game that relies on interaction. I was able to focus on underlying methodologies of the ClojureScript language and their relationship with my existing web development knowledge.

--------------------
Links
--------------------
ClojureScript > https://clojurescript.org

shadow-cljs > https://github.com/thheller/shadow-cljs

He loves me... he loves me not > https://en.wikipedia.org/wiki/He_loves_me..._he_loves_me_not

Atom > https://atom.io

Parinfer > https://shaunlebron.github.io/parinfer

Forget Me Not (a flower oracle) > https://thetechnoodle.com


March, 2021